CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE "users" (
  "id" UUID PRIMARY KEY DEFAULT uuid_generate_v1(),
  "username" VARCHAR NOT NULL,
  "password" VARCHAR NOT NULL
);