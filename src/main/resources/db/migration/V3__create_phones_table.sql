CREATE TABLE "phones" (
  "id" UUID PRIMARY KEY DEFAULT uuid_generate_v1(),
  "number" VARCHAR NOT NULL,
  "contact_id"  UUID REFERENCES "contacts" ON DELETE CASCADE ON UPDATE RESTRICT NOT NULL
);


