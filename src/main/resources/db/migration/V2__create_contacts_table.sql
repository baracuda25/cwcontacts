CREATE TABLE "contacts" (
  "id" UUID PRIMARY KEY DEFAULT uuid_generate_v1(),
  "first_name" VARCHAR NOT NULL,
  "last_name" VARCHAR NOT NULL,
  "user_id"  UUID REFERENCES "users" ON DELETE CASCADE ON UPDATE RESTRICT NOT NULL
);