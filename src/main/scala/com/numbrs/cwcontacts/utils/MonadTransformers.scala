package com.numbrs.cwcontacts.utils

import scala.concurrent.{ExecutionContext, Future}

/**
  * Monad transformers its classes that extends default monad's like Future or Option.
  * They handle situation when monad's contain each other, its helping to reduce amount of boilerplate code.
  * For example in that project there are lots of Future[Option[T]] classes that must be handled somehow.
  */
object MonadTransformers {

  implicit class FutureOptionMonadTransformer[A](t: Future[Option[A]])(implicit executionContext: ExecutionContext) {


    def filterT(f: A => Boolean): Future[Option[A]] =
      t.map {
        case Some(data) if f(data) =>
          Some(data)
        case _ =>
          None
      }


    def flatMapTInner[B](f: A => Option[B]): Future[Option[B]] =
      t.map(_.flatMap(f))

  }

}
