package com.numbrs.cwcontacts.utils

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directive1
import akka.http.scaladsl.server.directives.{BasicDirectives, HeaderDirectives, RouteDirectives}
import com.numbrs.cwcontacts.core.{AuthTokenContent, Id}
import io.circe.parser._
import pdi.jwt.{Jwt, JwtAlgorithm}
import io.circe.generic.auto._

object SecurityDirectives {

  import BasicDirectives._
  import HeaderDirectives._
  import RouteDirectives._

  def authenticate(secretKey: String): Directive1[Id] = {
    headerValueByName("Token")
      .map(Jwt.decodeRaw(_, secretKey, Seq(JwtAlgorithm.HS256)))
      .map(_.toOption.flatMap(decode[AuthTokenContent](_).toOption))
      .flatMap {
        case Some(result) =>
          provide(result.userId)
        case None =>
          complete(StatusCodes.Unauthorized)
      }
  }

}
