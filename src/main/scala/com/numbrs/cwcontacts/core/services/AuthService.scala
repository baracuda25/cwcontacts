package com.numbrs.cwcontacts.core.services

import com.numbrs.cwcontacts.core._
import com.numbrs.cwcontacts.utils.MonadTransformers._
import io.circe.generic.auto._
import io.circe.syntax._
import pdi.jwt.{Jwt, JwtAlgorithm}

import scala.concurrent.{ExecutionContext, Future}

class AuthService(userService: UserService, secretKey: String
                 )(implicit executionContext: ExecutionContext) {

  def signIn(user: User): Future[Option[AuthToken]] =
    userService.getByName(user.username)
      .filterT(_.password == user.password)
      .flatMapTInner(user => user.id.map(encodeToken))


  def encodeToken(userId: Id): AuthToken =
    AuthToken(Jwt.encode(AuthTokenContent(userId).asJson.noSpaces, secretKey, JwtAlgorithm.HS256))

}