package com.numbrs.cwcontacts.core.services

import com.numbrs.cwcontacts.core.db.ContactStorage
import com.numbrs.cwcontacts.core.{Contact, Id, PhoneNumber, ValidationError}

import scala.concurrent.{ExecutionContext, Future}

class ContactService(contactStorage: ContactStorage)
                    (implicit executionContext: ExecutionContext) {

  def delete(contactId: Id, userId: Id): Future[Int] =
    contactStorage.delete(contactId, userId)

  def update(contact: Contact): Future[Int] =
    contactStorage.update(contact)

  def create(contact: Contact): Future[Id] =
    contactStorage.create(contact)

  def findAll(userId: Id): Future[Seq[(Contact,Seq[PhoneNumber])]] = contactStorage.findAll(userId)

  def addContactPhoneNumber(userId: Id, phone: PhoneNumber) : Future[Either[ValidationError, Id]] =
    contactStorage.create(userId, phone)
}
