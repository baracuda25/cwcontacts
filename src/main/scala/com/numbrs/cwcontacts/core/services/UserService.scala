package com.numbrs.cwcontacts.core.services

import com.numbrs.cwcontacts.core.db.UserStorage
import com.numbrs.cwcontacts.core.{Id, User, ValidationError}

import scala.concurrent.{ExecutionContext, Future}

class UserService(userStorage: UserStorage)
                 (implicit executionContext: ExecutionContext) {

  def get(id: Id): Future[Option[User]] =
    userStorage.get(id)

  def getByName(username: String): Future[Option[User]] =
    userStorage.getByName(username)

  def create(user: User): Future[Either[ValidationError, Id]] =
    userStorage.create(user)
}
