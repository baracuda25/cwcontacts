package com.numbrs.cwcontacts

import java.util.UUID

package object core {

  type Id = UUID

  final case class AuthToken(token: String)

  final case class AuthTokenContent(userId: Id)

  final case class ValidationError(description: String)

  final case class User(username: String, password: String, id: Option[Id] = None) {
    require(username.nonEmpty, "username.empty")
    require(password.nonEmpty, "password.empty")
  }

  final case class Contact(firstName: String, lastName: String, userId : Id, id: Option[Id] = None) {
    require(firstName.nonEmpty, "firstName.empty")
    require(lastName.nonEmpty, "lastName.empty")
  }

  final case class PhoneNumber(number: String, contactId : Id, id: Option[Id] = None) {
    require(number.nonEmpty, "number.empty")
  }

}
