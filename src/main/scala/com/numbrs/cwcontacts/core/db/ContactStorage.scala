package com.numbrs.cwcontacts.core.db

import java.util.UUID

import com.numbrs.cwcontacts.core._
import com.numbrs.cwcontacts.utils.db.DatabaseConnector

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future}

sealed trait ContactStorage {
  def findAll(userId: Id): Future[Seq[(Contact, Seq[PhoneNumber])]]

  def delete(contactId: Id, userId: Id): Future[Int]

  def create(contact: Contact): Future[Id]

  def update(contact: Contact): Future[Int]

  def create(userId: Id, phone: PhoneNumber): Future[Either[ValidationError, Id]]

}

class JdbcContactStorage(val databaseConnector: DatabaseConnector)
                        (implicit executionContext: ExecutionContext) extends Schema with ContactStorage {

  import databaseConnector._
  import databaseConnector.profile.api._

  override def findAll(userId: Id): Future[Seq[(Contact, Seq[PhoneNumber])]] = {
    val findContacts = contacts
      .joinLeft(phoneNumbers).on(_.id === _.contactId)
    db.run(findContacts
      .filter(_._1.userId === userId)
      .sortBy(_._1.firstName.desc.nullsLast)
      .result
      .map(r => r.groupBy(_._1)
        .mapValues(_.flatMap(_._2)).toSeq))
  }


  override def create(contact: Contact): Future[Id] = {
    db.run(contacts returning contacts.map(_.id) into ((_, id) => id) += contact)
  }

  override def delete(contactId: Id, userId: Id): Future[Int] =
    db.run(contacts.filter(row => row.id === contactId && row.userId === userId).delete)

  override def update(contact: Contact): Future[Int] = db.run(
    contacts.filter(row => row.id === contact.id && row.userId === contact.userId).update(contact)
  )

  override def create(userId: Id, phone: PhoneNumber): Future[Either[ValidationError, Id]] = {
    val insertAction = contacts.filter(row => row.id === phone.contactId
      && row.userId === userId).result.headOption.flatMap {
      case Some(exists) =>
        val phoneId =
          (phoneNumbers returning phoneNumbers.map(_.id)) += phone
        phoneId.map { id => Right(id) }
      case None =>
        DBIO.failed(new IllegalArgumentException(s"Contact with id ${phone.contactId}" +
          s" doesn't exists or doesn't belong to you!"))
    }.transactionally
    db.run(insertAction).recover({
      case e: IllegalArgumentException => Left(ValidationError(e.getMessage))
    })
  }
}

class InMemoryContactStorage extends ContactStorage {

  private var state: ListBuffer[Contact] = ListBuffer()
  private var phoneState: Seq[PhoneNumber] = Nil

  override def findAll(userId: Id): Future[Seq[(Contact, Seq[PhoneNumber])]] =
    Future.successful(state.filter(_.userId == userId)
      .map(c => (c, phoneState.filter(p => c.id.contains(p.contactId)))).toList)

  override def create(contact: Contact): Future[Id] = {
    Future.successful {
      val id = UUID.randomUUID()
      state = state :+ contact.copy(id = Some(id))
      id
    }
  }

  override def delete(contactId: Id, userId: Id): Future[Int] = {
    Future.successful {
      state.find(c => c.userId == userId && c.id.get == contactId).map(c => {
        state -= c
        1
      }).getOrElse(0)
    }
  }

  override def update(contact: Contact): Future[Int] = Future.successful {
    state.find(c => c.userId == contact.userId && c.id == contact.id).map(c => {
      state -= c
      state += contact
      1
    }).getOrElse(0)
  }

  override def create(userId: Id, phone: PhoneNumber): Future[Either[ValidationError, Id]] =
    Future.successful {
      val id = UUID.randomUUID()
      phoneState = phoneState :+ phone.copy(id = Some(id))
      Right(id)
    }
}