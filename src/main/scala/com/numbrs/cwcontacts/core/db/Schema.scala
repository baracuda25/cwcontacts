package com.numbrs.cwcontacts.core.db

import com.numbrs.cwcontacts.core.{Contact, Id, PhoneNumber, User}
import com.numbrs.cwcontacts.utils.db.DatabaseConnector

private[db] trait Schema {

  protected val databaseConnector: DatabaseConnector

  import databaseConnector.profile.api._

  class Users(tag: Tag) extends Table[User](tag, "users") {
    def id = column[Id]("id", O.PrimaryKey, O.AutoInc)

    def userName = column[String]("username")

    def password = column[String]("password")

    def * = (userName, password, id.?) <> (User.tupled, User.unapply)
  }

  class Contacts(tag: Tag) extends Table[Contact](tag, "contacts") {
    def id = column[Id]("id", O.PrimaryKey, O.AutoInc)

    def firstName = column[String]("first_name")

    def lastName = column[String]("last_name")

    def userId = column[Id]("user_id")

    def * = (firstName, lastName, userId, id.?) <> (Contact.tupled, Contact.unapply)

    def user =
      foreignKey("contacts_user_id_fkey", userId, users)(_.id,
        onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Cascade)
  }

  class PhoneNumbers(tag: Tag) extends Table[PhoneNumber](tag, "phones") {
    def id = column[Id]("id", O.PrimaryKey, O.AutoInc)

    def number = column[String]("number")

    def contactId = column[Id]("contact_id")

    def * = (number, contactId, id.?) <> (PhoneNumber.tupled, PhoneNumber.unapply)

    def contact = foreignKey("phones_contact_id_fkey", contactId,
      users)(_.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Cascade)
  }


  val users = TableQuery[Users]
  val contacts = TableQuery[Contacts]
  val phoneNumbers = TableQuery[PhoneNumbers]

}
