package com.numbrs.cwcontacts.core.db

import java.util.UUID

import com.numbrs.cwcontacts.core.{Id, User, ValidationError}
import com.numbrs.cwcontacts.utils.db.DatabaseConnector

import scala.concurrent.{ExecutionContext, Future}

sealed trait UserStorage {
  def get(id: Id): Future[Option[User]]

  def getByName(userName: String): Future[Option[User]]

  def create(user: User): Future[Either[ValidationError, Id]]
}

class JdbcUserStorage(val databaseConnector: DatabaseConnector)
                     (implicit executionContext: ExecutionContext) extends Schema with UserStorage {

  import databaseConnector._
  import databaseConnector.profile.api._

  override def get(id: Id): Future[Option[User]] = db.run(users.filter(_.id === id).result.headOption)

  override def getByName(userName: String): Future[Option[User]] = db.run(users.filter(_.userName === userName).result.headOption)

  override def create(user: User): Future[Either[ValidationError, Id]] = {
    val userAction = users.filter(_.userName === user.username).result.headOption.flatMap {
      case Some(exists) =>
        DBIO.failed(new IllegalArgumentException(s"User ${exists.username} already exists!"))
      case None =>
        val userId =
          (users returning users.map(_.id)) += user
        userId.map { id => Right(id) }
    }.transactionally
    db.run(userAction).recover({
      case e: IllegalArgumentException => Left(ValidationError(e.getMessage))
    })
  }
}

class InMemoryUserStorage extends UserStorage {

  private var state: Seq[User] = Nil

  override def get(id: Id): Future[Option[User]] =
    Future.successful(state.find(_.id.contains(id)))

  override def getByName(userName: String): Future[Option[User]] =
    Future.successful(state.find(_.username == userName))

  override def create(user: User): Future[Either[ValidationError, Id]] =
    Future.successful {
      val id = UUID.randomUUID()
      val value = state.find(_.username == user.username)
      if (value.isDefined) {
        Left(ValidationError(s"User ${value.get.username} already exists!"))
      } else {
        state = state :+ user.copy(id = Some(id))
        Right(id)
      }
    }

}