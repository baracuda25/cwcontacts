package com.numbrs.cwcontacts

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.numbrs.cwcontacts.core.db.{JdbcContactStorage, JdbcUserStorage}
import com.numbrs.cwcontacts.core.services.{AuthService, ContactService, UserService}
import com.numbrs.cwcontacts.http.HttpRoute
import com.numbrs.cwcontacts.utils.Config
import com.numbrs.cwcontacts.utils.db.{DatabaseConnector, DatabaseMigrationManager}
import slick.util.Logging

import scala.concurrent.ExecutionContext

object Boot extends App with Logging {

  def startApplication() = {

    implicit val actorSystem = ActorSystem()
    implicit val executor: ExecutionContext = actorSystem.dispatcher
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    val config = Config.load()

    new DatabaseMigrationManager(
      config.database.jdbcUrl,
      config.database.username,
      config.database.password
    ).migrateDatabaseSchema()

    val databaseConnector = new DatabaseConnector(
      config.database.jdbcUrl,
      config.database.username,
      config.database.password
    )

    val userStorage = new JdbcUserStorage(databaseConnector)
    val contactStorage = new JdbcContactStorage(databaseConnector)

    val usersService = new UserService(userStorage)
    val contactService = new ContactService(contactStorage)
    val authService = new AuthService(usersService, config.secretKey)
    val httpRoute = new HttpRoute(usersService, authService, contactService, config.secretKey)

    Http().bindAndHandle(httpRoute.route, config.http.host, config.http.port)
  }

  startApplication()

}
