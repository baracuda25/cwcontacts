package com.numbrs.cwcontacts.http.routes


import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import com.numbrs.cwcontacts.core.services.{AuthService, UserService}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import io.circe.syntax._
import slick.util.Logging

import scala.concurrent.ExecutionContext

class UserRoute(
                 secretKey: String,
                 usersService: UserService,
                 authService: AuthService
               )(implicit executionContext: ExecutionContext) extends FailFastCirceSupport with Logging {

  val route = pathPrefix("users") {
    pathEndOrSingleSlash {
      post {
        entity(as[User]) { userEntity =>
          complete(usersService.create(userEntity.toDomain)
            .map(result => result.fold(
              error => {
                logger.info(s"Unable to register a user ${userEntity.username}" +
                  s" due to error '${error.description}'")
                StatusCodes.BadRequest -> error.asJson
              },
              id => {
                logger.info(s"Successfully registered ${userEntity.username}")
                StatusCodes.Created -> Nil.asJson
              }
            )))
        }
      }
    } ~
      path("sessions") {
        pathEndOrSingleSlash {
          post {
            entity(as[User]) { userEntity =>
              complete(authService.signIn(userEntity.toDomain).map {
                case Some(token) =>
                  logger.info(s"User ${userEntity.username} succesfully signed in")
                  StatusCodes.OK -> token.asJson
                case _ =>
                  logger.info(s"User ${userEntity.username} failed to sign in.")
                  StatusCodes.Unauthorized -> Nil.asJson
              })
            }
          }
        }
      }
  }

}
