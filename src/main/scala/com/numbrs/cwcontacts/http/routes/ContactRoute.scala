package com.numbrs.cwcontacts.http.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import com.numbrs.cwcontacts.core.Id
import com.numbrs.cwcontacts.core.services.ContactService
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import io.circe.syntax._
import slick.util.Logging

import scala.concurrent.ExecutionContext

class ContactRoute(secretKey: String,
                   contactService: ContactService
                  )(implicit executionContext: ExecutionContext) extends FailFastCirceSupport with Logging {

  import com.numbrs.cwcontacts.utils.SecurityDirectives._

  val route = pathPrefix("contacts") {
    pathEndOrSingleSlash {
      post {
        authenticate(secretKey) { userId =>
          entity(as[Contact]) { contactEntity =>
            complete(StatusCodes.Created -> contactService.create(Contact.to(contactEntity, userId)).map(id => {
              logger.info(s"Succesfully created contact with id $id for user $userId")
              EntityId(id)
            }
            ))
          }
        }
      } ~ get {
        authenticate(secretKey) { userId =>
          complete(StatusCodes.OK -> contactService.findAll(userId).map(_.map(Contact.from)))
        }
      }
    } ~
      pathPrefix(JavaUUID) { id: Id =>
        pathEndOrSingleSlash {
          delete {
            authenticate(secretKey) { userId =>
              complete(
                contactService.delete(id, userId)
                  .map {
                    case count if count > 0 =>
                      logger.info(s"Succesfully removed contact with id $id for user $userId")
                      StatusCodes.NoContent -> Nil.asJson
                    case _ =>
                      logger.info(s"Unable to delete the contact with id $id for user $userId")
                      StatusCodes.NotFound -> Nil.asJson
                  }
              )
            }
          } ~
            post {
              authenticate(secretKey) { userId =>
                entity(as[Contact]) { contactEntity =>
                  complete(
                    contactService.update(Contact.to(contactEntity, userId, Some(id)))
                      .map {
                        case count if count > 0 =>
                          logger.info(s"Succesfully updated contact with id $id for user $userId")
                          StatusCodes.OK -> EntityId(id).asJson
                        case _ =>
                          logger.info(s"Unable to update the contact with id $id for user $userId")
                          StatusCodes.NotFound -> Nil.asJson
                      }
                  )
                }
              }
            }
        } ~ path("entries") {
          pathEndOrSingleSlash {
            post {
              authenticate(secretKey) { userId =>
                entity(as[PhoneNumber]) { phoneEntity =>
                  complete(contactService.addContactPhoneNumber(userId, PhoneNumber.to(phoneEntity, id))
                    .map(result => result.fold(
                      error => {
                        logger.info(s"Adding a phone number to contact with id $id for user $userId" +
                          s" finished with error ${error.description}")
                        StatusCodes.BadRequest -> error.asJson
                      },
                      phoneId => {
                        logger.info(s"Successfully added phone number $phoneId" +
                          s" for user = $userId and contact $id")
                        StatusCodes.Created -> EntityId(id).asJson
                      }
                    )))
                }
              }
            }
          }
        }
      }
  }
}


