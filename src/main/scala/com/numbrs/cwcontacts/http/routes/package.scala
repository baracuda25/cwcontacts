package com.numbrs.cwcontacts.http

import com.google.i18n.phonenumbers.{PhoneNumberUtil, Phonenumber}
import com.numbrs.cwcontacts.core
import com.numbrs.cwcontacts.core.Id
import com.roundeights.hasher.Implicits._

package object routes {

  final case class User(username: String, password: String) {
    require(username.nonEmpty, "username.empty")
    require(password.nonEmpty, "password.empty")

    def toDomain : core.User = core.User(username,password.sha256.hex)
  }


  final case class Contact(first_name: String, last_name: String, id: Option[Id] = None, phones: Option[Seq[String]]) {
    require(first_name.nonEmpty, "first_name.empty")
    require(last_name.nonEmpty, "last_name.empty")
  }

  object Contact {
    def from(tuple: (core.Contact, Seq[core.PhoneNumber])): Contact = tuple match {
      case (contact, numbers) => Contact(contact.firstName, contact.lastName, contact.id, Some(numbers.map(PhoneNumber.from)))
    }

    def to(contact: Contact, userId: Id, id: Option[Id] = None) = core.Contact(contact.first_name, contact.last_name, userId, id)
  }

  final case class PhoneNumber(phone: String) {
    require(phone.nonEmpty, "number.empty")
    private val parsedNumber: Phonenumber.PhoneNumber = PhoneNumberUtil.getInstance().parse(phone, "+41")
    require(PhoneNumberUtil.getInstance().isPossibleNumber(parsedNumber), "number.notValid")
  }

  object PhoneNumber {
    def to(phone: PhoneNumber, contactId: Id): core.PhoneNumber = core.PhoneNumber(phone.phone, contactId)

    def from(phone: core.PhoneNumber): String = phone.number
  }

  final case class EntityId(id: Id)

}
