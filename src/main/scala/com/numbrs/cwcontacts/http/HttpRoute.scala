package com.numbrs.cwcontacts.http

import akka.http.scaladsl.server.Route
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import com.numbrs.cwcontacts.core.services.{AuthService, ContactService, UserService}
import com.numbrs.cwcontacts.http.routes.{ContactRoute, UserRoute}
import akka.http.scaladsl.server.Directives._

import scala.concurrent.ExecutionContext

class HttpRoute(userProfileService: UserService,
                authService: AuthService,
                contactService: ContactService,
                secretKey: String
               )(implicit executionContext: ExecutionContext) {

  private val usersRouter = new UserRoute(secretKey, userProfileService, authService)
  private val contactsRouter = new ContactRoute(secretKey, contactService)


  val route: Route =
    cors() {
      usersRouter.route ~ contactsRouter.route ~
        pathPrefix("healthcheck") {
          get {
            complete("OK")
          }
        }
    }


}
