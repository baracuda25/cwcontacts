package com.numbrs.cwcontacts.core.services

import java.util.UUID

import com.numbrs.cwcontacts.BaseServiceTest
import com.numbrs.cwcontacts.core.db.InMemoryContactStorage
import com.numbrs.cwcontacts.core.{Contact, PhoneNumber}

class ContactServiceTest extends BaseServiceTest {
  "ContactService" when {
    "create" should {
      "create multiple contacts in a row" in new Context {
        awaitForResult(for {
          _ <- contactService.create(testContact1)
          _ <- contactService.create(testContact2)
          contacts <- contactService.findAll(testUserId)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2))
          shouldBe Seq((testContact1, Seq()), (testContact2, Seq())))
      }

      "create multiple contacts in a row and phone numbers" in new Context {
        awaitForResult(for {
          contact1Id <- contactService.create(testContact1)
          _ <- contactService.create(testContact2)
          _ <- contactService.addContactPhoneNumber(contact1Id, PhoneNumber("test", contact1Id))
          contacts <- contactService.findAll(testUserId)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2.map(_.copy(id = None))))
          shouldBe Seq((testContact1, Seq(PhoneNumber("test", contact1Id))), (testContact2, Seq())))
      }
    }
    "findAll" should {
      "return list of contacts by id" in new Context {
        awaitForResult(for {
          _ <- contactService.create(testContact1)
          _ <- contactService.create(testContact2)
          contacts <- contactService.findAll(testUserId)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2))
          shouldBe Seq((testContact1, Seq()), (testContact2, Seq())))
      }
      "return empty list by id" in new Context {
        awaitForResult(for {
          _ <- contactService.create(testContact1)
          _ <- contactService.create(testContact2)
          contacts <- contactService.findAll(testId)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2))
          shouldBe Seq.empty)
      }
    }
    "delete" should {
      "delete succesfully" in new Context {
        awaitForResult(for {
          testContactId1 <- contactService.create(testContact1)
          _ <- contactService.create(testContact2)
          _ <- contactService.delete(testContactId1, testUserId)
          contacts <- contactService.findAll(testUserId)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2))
          shouldBe Seq((testContact2, Seq())))
      }
      "delete contact which doesn't exists" in new Context {
        awaitForResult(for {
          _ <- contactService.create(testContact1)
          _ <- contactService.create(testContact2)
          _ <- contactService.delete(testId, testUserId)
          contacts <- contactService.findAll(testUserId)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2))
          shouldBe Seq((testContact1, Seq()), (testContact2, Seq())))
      }
    }

    "update" should {
      "update succesfully" in new Context {
        awaitForResult(for {
          testContactId1 <- contactService.create(testContact1)
          _ <- contactService.create(testContact2)
          _ <- contactService.update(testContact1.copy(id = Some(testContactId1), firstName = "bla"))
          contacts <- contactService.findAll(testUserId)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2))
          shouldBe Seq((testContact2, Seq()), (testContact1.copy(firstName = "bla"), Seq())))
      }
      "update of contact which doesn't exists" in new Context {
        awaitForResult(for {
          _ <- contactService.create(testContact1)
          _ <- contactService.create(testContact2)
          _ <- contactService.update(testContact1.copy(userId = testId, firstName = "bla"))
          contacts <- contactService.findAll(testUserId)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2))
          shouldBe Seq((testContact1, Seq()), (testContact2, Seq())))
      }
    }
  }

  trait Context {
    val contactStorage = new InMemoryContactStorage()
    val contactService = new ContactService(contactStorage)
    val testId: UUID = UUID.randomUUID()
    val testUserId: UUID = UUID.randomUUID()
    val testContact1: Contact = Contact("test", "testPass", testUserId)
    val testContact2: Contact = Contact("test1", "testPass1", testUserId)
  }

}
