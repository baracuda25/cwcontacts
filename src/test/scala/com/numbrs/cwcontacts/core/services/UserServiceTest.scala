package com.numbrs.cwcontacts.core.services

import java.util.UUID

import com.numbrs.cwcontacts.BaseServiceTest
import com.numbrs.cwcontacts.core.{User, ValidationError}
import com.numbrs.cwcontacts.core.db.InMemoryUserStorage

class UserServiceTest extends BaseServiceTest {

  "UserService" when {

    "get" should {

      "get by id" should {
        "return Some(user) by id if user exists" in new Context {
          awaitForResult(for {
            createdUser1 <- userService.create(testUser1)
            _ <- userService.create(testUser2)
            maybeProfile <- userService.get(createdUser1.toOption.get)
          } yield maybeProfile.map(_.copy(id = None)) shouldBe Some(testUser1))
        }

        "return None if user doesn't exists" in new Context {
          awaitForResult(for {
            _ <- userService.create(testUser1)
            _ <- userService.create(testUser2)
            maybeProfile <- userService.get(testId)
          } yield maybeProfile shouldBe None)
        }
      }

      "get by name" should {
        "return Some(user) by name if user exists" in new Context {
          awaitForResult(for {
            _ <- userService.create(testUser1)
            _ <- userService.create(testUser2)
            maybeProfile <- userService.getByName(testUser1.username)
          } yield maybeProfile.map(_.copy(id = None)) shouldBe Some(testUser1))
        }

        "return None if user doesn't exists" in new Context {
          awaitForResult(for {
            _ <- userService.create(testUser1)
            _ <- userService.create(testUser2)
            maybeProfile <- userService.getByName("bla")
          } yield maybeProfile shouldBe None)
        }
      }
    }


    "create" should {
      "store user if it doesn't exists" in new Context {
        awaitForResult(for {
          createdUser1 <- userService.create(testUser1)
          maybeProfile <- userService.get(createdUser1.toOption.get)
        } yield maybeProfile.map(_.copy(id = None)) shouldBe Some(testUser1))
      }
      "fail when user already exists" in new Context {
        awaitForResult(for {
          _ <- userService.create(testUser1)
          result <- userService.create(testUser1)
        } yield result shouldBe Left(ValidationError("User test already exists!")))
      }
    }
  }

  trait Context {
    val userStorage = new InMemoryUserStorage()
    val userService = new UserService(userStorage)
    val testId: UUID = UUID.randomUUID()
    val testUser1: User = User("test", "testPass")
    val testUser2: User = User("test1", "testPass1")
  }


}
