package com.numbrs.cwcontacts.core.services

import java.util.UUID

import com.numbrs.cwcontacts.BaseServiceTest
import com.numbrs.cwcontacts.core.{AuthTokenContent, User}
import io.circe.generic.auto._
import io.circe.parser._
import org.mockito.Mockito._
import pdi.jwt.{Jwt, JwtAlgorithm}

import scala.concurrent.Future

class AuthServiceTest extends BaseServiceTest {

  "AuthService" when {
    "signIn" should {
      "return token if user exists" in new Context {
        when(userService.getByName(testUser.username))
          .thenReturn(Future.successful(Some(testUser)))
        awaitForResult(for {
          maybeToken <- authService.signIn(testUser)
        } yield {
          val maybeContent = maybeToken
            .map(token => Jwt.decodeRaw(token.token, secretKey, Seq(JwtAlgorithm.HS256)))
            .flatMap(_.toOption.flatMap(decode[AuthTokenContent](_).toOption))
          maybeContent shouldBe Some(AuthTokenContent(testId))
        })
      }

      "not return token if user doesn't exists" in new Context {
        when(userService.getByName(testUser.username))
          .thenReturn(Future.successful(None))
        awaitForResult(for {
          maybeToken <- authService.signIn(testUser)
        } yield maybeToken shouldBe None)
      }
    }
  }

  trait Context {
    val secretKey = "secret"
    val userService = mock[UserService]
    val authService = new AuthService(userService, secretKey)
    val testId: UUID = UUID.randomUUID()
    val testUser: User = User("test", "testPass", Some(testId))
  }


}
