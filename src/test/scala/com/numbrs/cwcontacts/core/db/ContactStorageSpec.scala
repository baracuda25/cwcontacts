package com.numbrs.cwcontacts.core.db

import java.util.UUID

import com.numbrs.cwcontacts.BaseServiceTest
import com.numbrs.cwcontacts.core.{Contact, PhoneNumber, User}
import com.numbrs.cwcontacts.utils.InMemoryPostgresStorage


class JdbcContactStorageTest extends ContactStorageSpec {
  override def contactStorageBuilder(): ContactStorage =
    new JdbcContactStorage(InMemoryPostgresStorage.databaseConnector)

  override def userStorageBuilder(): UserStorage =
    new JdbcUserStorage(InMemoryPostgresStorage.databaseConnector)
}

class InMemoryContactStorageTest extends ContactStorageSpec {
  override def contactStorageBuilder(): ContactStorage =
    new InMemoryContactStorage()

  override def userStorageBuilder(): UserStorage = new InMemoryUserStorage()
}

abstract class ContactStorageSpec extends BaseServiceTest {

  def contactStorageBuilder(): ContactStorage

  def userStorageBuilder(): UserStorage

  "ContactService" when {
    "create" should {
      "create multiple contacts in a row" in new Context {
        awaitForResult(for {
          user <- userStorage.create(User("test1","pass1"))
          _ <- contactStorage.create(testContact1.copy(userId = user.toOption.get))
          _ <- contactStorage.create(testContact2.copy(userId = user.toOption.get))
          contacts <- contactStorage.findAll(user.toOption.get)
        } yield contacts
          .map(tuple => (tuple._1.copy(id = None), tuple._2))
          .sortBy(_._1.firstName)
          shouldBe Seq((testContact1.copy(userId = user.toOption.get), Seq()),
          (testContact2.copy(userId = user.toOption.get), Seq())))
      }

      "create multiple contacts in a row and phone numbers" in new Context {
        awaitForResult(for {
          user <- userStorage.create(User("test2","pass2"))
          contact1Id <- contactStorage.create(testContact1.copy(userId = user.toOption.get))
          _ <- contactStorage.create(testContact2.copy(userId = user.toOption.get))
          _ <- contactStorage.create(user.toOption.get, PhoneNumber("test", contact1Id))
          contacts <- contactStorage.findAll(user.toOption.get)
        } yield contacts
          .map(tuple => (tuple._1.copy(id = None), tuple._2.map(_.copy(id = None))))
          .sortBy(_._1.firstName)
          shouldBe Seq((testContact1.copy(userId = user.toOption.get),Seq(PhoneNumber("test", contact1Id))),
          (testContact2.copy(userId = user.toOption.get), Seq())
        ))
      }
    }
    "findAll" should {
      "return list of contacts by id" in new Context {
        awaitForResult(for {
          user <- userStorage.create(User("test3","pass3"))
          _ <- contactStorage.create(testContact1.copy(userId = user.toOption.get))
          _ <- contactStorage.create(testContact2.copy(userId = user.toOption.get))
          contacts <- contactStorage.findAll(user.toOption.get)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2)) .sortBy(_._1.firstName)
          shouldBe Seq((testContact1.copy(userId = user.toOption.get), Seq()),
          (testContact2.copy(userId = user.toOption.get), Seq())))
      }
      "return empty list by id" in new Context {
        awaitForResult(for {
          user <- userStorage.create(User("test4","pass4"))
          _ <- contactStorage.create(testContact1.copy(userId = user.toOption.get))
          _ <- contactStorage.create(testContact2.copy(userId = user.toOption.get))
          contacts <- contactStorage.findAll(testId)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2))
          shouldBe Seq.empty)
      }
    }
    "delete" should {
      "delete succesfully" in new Context {
        awaitForResult(for {
          user <- userStorage.create(User("test5","pass5"))
          testContactId1 <- contactStorage.create(testContact1.copy(userId = user.toOption.get))
          _ <- contactStorage.create(testContact2.copy(userId = user.toOption.get))
          _ <- contactStorage.delete(testContactId1, user.toOption.get)
          contacts <- contactStorage.findAll(user.toOption.get)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2))
          shouldBe Seq((testContact2.copy(userId = user.toOption.get), Seq())))
      }
      "delete contact which doesn't exists" in new Context {
        awaitForResult(for {
          user <- userStorage.create(User("test6","pass6"))
          _ <- contactStorage.create(testContact1.copy(userId = user.toOption.get))
          _ <- contactStorage.create(testContact2.copy(userId = user.toOption.get))
          _ <- contactStorage.delete(testId, user.toOption.get)
          contacts <- contactStorage.findAll(user.toOption.get)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2)) .sortBy(_._1.firstName)
          shouldBe Seq((testContact1.copy(userId = user.toOption.get), Seq()),
          (testContact2.copy(userId = user.toOption.get), Seq())))
      }
    }

    "update" should {
      "update succesfully" in new Context {
        awaitForResult(for {
          user <- userStorage.create(User("test7","pass7"))
          testContactId1 <- contactStorage.create(testContact1.copy(userId = user.toOption.get))
          _ <- contactStorage.create(testContact2.copy(userId = user.toOption.get))
          _ <- contactStorage.update(testContact1.copy(userId = user.toOption.get,
            id = Some(testContactId1), firstName = "bla"))
          contacts <- contactStorage.findAll(user.toOption.get)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2)) .sortBy(_._1.firstName)
          shouldBe Seq((testContact1.copy(firstName = "bla",userId = user.toOption.get), Seq()),
          (testContact2.copy(userId = user.toOption.get), Seq())))
      }
      "update of contact which doesn't exists" in new Context {
        awaitForResult(for {
          user <- userStorage.create(User("test8","pass8"))
          testContactId1 <- contactStorage.create(testContact1.copy(userId = user.toOption.get))
          _ <- contactStorage.create(testContact2.copy(userId = user.toOption.get))
          _ <- contactStorage.update(testContact1.copy(id = Some(testContactId1),userId = testId, firstName = "bla"))
          contacts <- contactStorage.findAll(user.toOption.get)
        } yield contacts.map(tuple => (tuple._1.copy(id = None), tuple._2)) .sortBy(_._1.firstName)
          shouldBe Seq((testContact1.copy(userId = user.toOption.get), Seq()),
          (testContact2.copy(userId = user.toOption.get), Seq())))
      }
    }
  }

  trait Context {
    val contactStorage: ContactStorage = contactStorageBuilder()
    val userStorage: UserStorage = userStorageBuilder()
    val testUser1: User = User("test1", "testPass1")
    val testId: UUID = UUID.randomUUID()
    val testContact1: Contact = Contact("test", "testPass", UUID.randomUUID())
    val testContact2: Contact = Contact("test1", "testPass1", UUID.randomUUID())
  }

}
