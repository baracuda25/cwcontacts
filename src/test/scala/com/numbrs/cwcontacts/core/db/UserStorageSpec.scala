package com.numbrs.cwcontacts.core.db

import java.util.UUID

import com.numbrs.cwcontacts.BaseServiceTest
import com.numbrs.cwcontacts.core.{User, ValidationError}
import com.numbrs.cwcontacts.utils.InMemoryPostgresStorage


class JdbcUserStorageTest extends UserStorageSpec {
  override def userStorageBuilder(): UserStorage =
    new JdbcUserStorage(InMemoryPostgresStorage.databaseConnector)
}

class InMemoryUserStorageTest extends UserStorageSpec {
  override def userStorageBuilder(): UserStorage =
    new InMemoryUserStorage()
}

abstract class UserStorageSpec extends BaseServiceTest{

  def userStorageBuilder(): UserStorage

  "UserStorage" when {

    "get" should {

      "get by id" should {
        "return Some(user) by id if user exists" in new Context {
          awaitForResult(for {
            createdUser1 <- userStorage.create(testUser1)
            _ <- userStorage.create(testUser2)
            maybeProfile <- userStorage.get(createdUser1.toOption.get)
          } yield maybeProfile.map(_.copy(id = None)) shouldBe Some(testUser1))
        }

        "return None if user doesn't exists" in new Context {
          awaitForResult(for {
            _ <- userStorage.create(testUser1)
            _ <- userStorage.create(testUser2)
            maybeProfile <- userStorage.get(testId)
          } yield maybeProfile shouldBe None)
        }
      }

      "get by name" should {
        "return Some(user) by name if user exists" in new Context {
          awaitForResult(for {
            _ <- userStorage.create(testUser1)
            _ <- userStorage.create(testUser2)
            maybeProfile <- userStorage.getByName(testUser1.username)
          } yield maybeProfile.map(_.copy(id = None)) shouldBe Some(testUser1))
        }

        "return None if user doesn't exists" in new Context {
          awaitForResult(for {
            _ <- userStorage.create(testUser1)
            _ <- userStorage.create(testUser2)
            maybeProfile <- userStorage.getByName("bla")
          } yield maybeProfile shouldBe None)
        }
      }
    }


    "create" should {
      "store user if it doesn't exists" in new Context {
        awaitForResult(for {
          createdUser1 <- userStorage.create(testUser3)
          maybeProfile <- userStorage.get(createdUser1.toOption.get)
        } yield maybeProfile.map(_.copy(id = None)) shouldBe Some(testUser3))
      }
      "fail when already exists" in new Context {
        awaitForResult(for {
          _ <- userStorage.create(testUser3)
          result <- userStorage.create(testUser3)
        } yield result shouldBe Left(ValidationError("User atest3 already exists!")))
      }
    }
  }

  trait Context {
    val userStorage: UserStorage = userStorageBuilder()

    val testId: UUID = UUID.randomUUID()
    val testUser1: User = User("atest1", "testPass1")
    val testUser2: User = User("atest2", "testPass2")
    val testUser3: User = User("atest3", "testPass3")
    val testUser4: User = User("atest4", "testPass4")

  }
}
