package com.numbrs.cwcontacts.http

import akka.http.scaladsl.server.Route
import com.numbrs.cwcontacts.BaseServiceTest
import com.numbrs.cwcontacts.core.services.{AuthService, ContactService, UserService}

class HttpRouteTest extends BaseServiceTest{
  "HttpRoute" when {

    "GET /healthcheck" should {

      "return 200 OK" in new Context {
        Get("/healthcheck") ~> httpRoute ~> check {
          responseAs[String] shouldBe "OK"
          status.intValue() shouldBe 200
        }
      }

    }

  }

  trait Context {
    val secretKey = "secret"
    val userService: UserService = mock[UserService]
    val contactService: ContactService = mock[ContactService]
    val authService: AuthService = mock[AuthService]

    val httpRoute: Route = new HttpRoute(userService, authService, contactService, secretKey).route
  }

}
