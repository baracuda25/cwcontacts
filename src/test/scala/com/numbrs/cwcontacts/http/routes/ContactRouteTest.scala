package com.numbrs.cwcontacts.http.routes

import java.util.UUID

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, MediaTypes}
import akka.http.scaladsl.server.{MissingHeaderRejection, Route, ValidationRejection}
import com.numbrs.cwcontacts.core.services.{AuthService, ContactService, UserService}
import com.numbrs.cwcontacts.core.{Id, ValidationError}
import com.numbrs.cwcontacts.{BaseServiceTest, core}
import io.circe.generic.auto._
import io.circe.syntax._
import org.mockito.Mockito._

import scala.concurrent.Future


class ContactRouteTest extends BaseServiceTest {

  "ContactRoute" when {

    "GET /contacts" should {

      "return 200 and all contatcs JSON" in new Context {
        val contacts = Seq((testContact1, Seq(testContact1PhoneNumber)), (testContact2, Seq()))
        when(contactService.findAll(userId))
          .thenReturn(Future.successful(contacts))

        val header = RawHeader("Token", authService.encodeToken(userId).token)

        Get("/contacts").withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe contacts.map(Contact.from).asJson.noSpaces
          status.intValue() shouldBe 200
          contentType shouldBe ContentTypes.`application/json`
        }
      }

      "rejected if token not provided" in new Context {
        val contacts = Seq((testContact1, Seq(testContact1PhoneNumber)), (testContact2, Seq()))
        when(contactService.findAll(userId))
          .thenReturn(Future.successful(contacts))

        Get("/contacts") ~> contactRoute ~> check {
          rejections contains MissingHeaderRejection("Token")
        }
      }

      "401 if token is invalid" in new Context {

        val header = RawHeader("Token", "test")

        Get("/contacts").withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe authError
          status.intValue() shouldBe 401
        }
      }

    }

    "POST /contacts" should {

      val requestEntity = HttpEntity(MediaTypes.`application/json`, s"""{"first_name": "Alice", "last_name": "Cooper"}""")


      "return 201 and created contact id" in new Context {
        when(contactService.create(testContact1.copy(id = None))).thenReturn(Future.successful(contact1Id))

        val header = RawHeader("Token", authService.encodeToken(userId).token)

        Post("/contacts", requestEntity).withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe EntityId(contact1Id).asJson.noSpaces
          contentType shouldBe ContentTypes.`application/json`
          status.intValue() shouldBe 201
        }
      }

      "401 if token is invalid" in new Context {

        val header = RawHeader("Token", "test")

        Post("/contacts", requestEntity).withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe authError
          status.intValue() shouldBe 401
        }
      }
    }

    "POST /contacts/contactId" should {

      val requestEntity = HttpEntity(MediaTypes.`application/json`, s"""{"first_name": "Alice", "last_name": "Cooper"}""")


      "return 200 if contact successfully updated" in new Context {
        when(contactService.update(testContact1)).thenReturn(Future.successful(1))

        val header = RawHeader("Token", authService.encodeToken(userId).token)

        Post(s"/contacts/$contact1Id", requestEntity).withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe EntityId(contact1Id).asJson.noSpaces
          contentType shouldBe ContentTypes.`application/json`
          status.intValue() shouldBe 200
        }
      }

      "return 404 if contact with given user and contact id doesn't exists" in new Context {
        when(contactService.update(testContact1)).thenReturn(Future.successful(0))

        val header = RawHeader("Token", authService.encodeToken(userId).token)

        Post(s"/contacts/$contact1Id", requestEntity).withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe "{}"
          contentType shouldBe ContentTypes.`application/json`
          status.intValue() shouldBe 404
        }
      }

      "401 if token is invalid" in new Context {

        val header = RawHeader("Token", "test")

        Post(s"/contacts/$contact1Id", requestEntity).withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe authError
          status.intValue() shouldBe 401
        }
      }
    }

    "DELETE /contacts/contactId" should {

      "return 204 if contact successfully removed" in new Context {

        when(contactService.delete(contact1Id,userId )).thenReturn(Future.successful(1))

        val header = RawHeader("Token", authService.encodeToken(userId).token)

        Delete(s"/contacts/$contact1Id").withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe ""
          status.intValue() shouldBe 204
        }
      }

      "return 404 if contact with given user and contact id doesn't exists" in new Context {
        when(contactService.delete(contact1Id,userId )).thenReturn(Future.successful(0))

        val header = RawHeader("Token", authService.encodeToken(userId).token)

        Delete(s"/contacts/$contact1Id").withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe "{}"
          contentType shouldBe ContentTypes.`application/json`
          status.intValue() shouldBe 404
        }
      }

      "401 if token is invalid" in new Context {

        val header = RawHeader("Token", "test")

        Delete(s"/contacts/$contact1Id").withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe authError
          status.intValue() shouldBe 401
        }
      }
    }

    "POST /contacts/contactId/entries" should {

      val requestEntity = HttpEntity(MediaTypes.`application/json`, s"""{"phone": "+49 1245 154875"}""")

      "return 201 if phone number was successfully added to contact" in new Context {
        when(contactService.addContactPhoneNumber(userId, testContact1PhoneNumber.copy(id = None)))
          .thenReturn(Future.successful(Right(phoneId)))

        val header = RawHeader("Token", authService.encodeToken(userId).token)

        Post(s"/contacts/$contact1Id/entries", requestEntity).withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe EntityId(contact1Id).asJson.noSpaces
          contentType shouldBe ContentTypes.`application/json`
          status.intValue() shouldBe 201
        }
      }

      "return 400 if phone number is malformed" in new Context {

        when(contactService.addContactPhoneNumber(userId, testContact1PhoneNumber.copy(id = None)))
          .thenReturn(Future.successful(Right(phoneId)))

        val header = RawHeader("Token", authService.encodeToken(userId).token)

        val malformedRequest = HttpEntity(MediaTypes.`application/json`, s"""{"phone": "+1 (555) 123-456"}""")

        Post(s"/contacts/$contact1Id/entries", malformedRequest).withHeaders(header) ~> contactRoute ~> check {
          rejection.asInstanceOf[ValidationRejection].message shouldBe "requirement failed: number.notValid"
        }
      }

      "return 400 if contact with given user and contact id doesn't exists or doesn't belong to a user" in new Context {

        val validationError = ValidationError("Contact doesn't exists!")

        when(contactService.addContactPhoneNumber(userId, testContact1PhoneNumber.copy(id = None)))
          .thenReturn(Future.successful(Left(validationError)))

        val header = RawHeader("Token", authService.encodeToken(userId).token)

        Post(s"/contacts/$contact1Id/entries", requestEntity).withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe validationError.asJson.noSpaces
          contentType shouldBe ContentTypes.`application/json`
          status.intValue() shouldBe 400
        }
      }

      "401 if token is invalid" in new Context {

        val header = RawHeader("Token", "test")

        Post(s"/contacts/$contact1Id/entries", requestEntity).withHeaders(header) ~> contactRoute ~> check {
          responseAs[String] shouldBe authError
          status.intValue() shouldBe 401
        }
      }
    }

  }

  trait Context {
    val secretKey = "secret"
    val contactService: ContactService = mock[ContactService]
    val userService: UserService = mock[UserService]
    val authService: AuthService = spy(new AuthService(userService,secretKey))
    val userId = UUID.randomUUID()
    val contact1Id: Id = UUID.randomUUID()
    val testContact1: core.Contact = core.Contact("Alice", "Cooper", userId, Some(contact1Id))
    val phoneId: Id = UUID.randomUUID()
    val testContact1PhoneNumber: core.PhoneNumber = core.PhoneNumber("+49 1245 154875", contact1Id, Some(phoneId))
    val testContact2: core.Contact = core.Contact("Donald", "Trump", userId, Some(UUID.randomUUID()))
    val contactRoute: Route = new ContactRoute(secretKey, contactService).route
    val authError = "Authentication is possible but has failed or not yet been provided."
  }

}


