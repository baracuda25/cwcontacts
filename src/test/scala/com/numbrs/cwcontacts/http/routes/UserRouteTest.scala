package com.numbrs.cwcontacts.http.routes

import java.util.UUID

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, MediaTypes}
import akka.http.scaladsl.server.Route
import com.numbrs.cwcontacts.BaseServiceTest
import com.numbrs.cwcontacts.core._
import com.numbrs.cwcontacts.core.services.{AuthService, UserService}
import io.circe.generic.auto._
import io.circe.syntax._
import org.mockito.Mockito._

import scala.concurrent.Future

class UserRouteTest extends BaseServiceTest {

  "UserRoute" when {

    val requestEntity = HttpEntity(MediaTypes.`application/json`, s"""{"username": "test", "password": "test"}""")

    "POST /users" should {

      "return 201" in new Context {
        when(userService.create(testUser.toDomain)).thenReturn(Future.successful(Right(userId)))

        Post("/users", requestEntity) ~> userRoute ~> check {
          responseAs[String] shouldBe "{}"
          contentType shouldBe ContentTypes.`application/json`
          status.intValue() shouldBe 201
        }
      }

      "return 400 if user already exists" in new Context {
        private val validationError = ValidationError("User already exists!")
        when(userService.create(testUser.toDomain))
          .thenReturn(Future.successful(Left(validationError)))
        Post("/users", requestEntity) ~> userRoute ~> check {
          responseAs[String] shouldBe validationError.asJson.noSpaces
          contentType shouldBe ContentTypes.`application/json`
          status.intValue() shouldBe 400
        }
      }
    }

    "POST /users/sessions" should {

      "return 200 if successfully signed in" in new Context {
        when(authService.signIn(testUser.toDomain)).thenReturn(Future.successful(Some(authToken)))

        Post("/users/sessions", requestEntity) ~> userRoute ~> check {
          responseAs[String] shouldBe authToken.asJson.noSpaces
          contentType shouldBe ContentTypes.`application/json`
          status.intValue() shouldBe 200
        }
      }

      "return 401 if unsuccessfully signed in" in new Context {
        when(authService.signIn(testUser.toDomain)).thenReturn(Future.successful(None))

        Post("/users/sessions", requestEntity) ~> userRoute ~> check {
          responseAs[String] shouldBe "{}"
          contentType shouldBe ContentTypes.`application/json`
          status.intValue() shouldBe 401
        }
      }
    }
  }

  trait Context {
    val secretKey = "secret"
    val userService: UserService = mock[UserService]
    val authService: AuthService = mock[AuthService]
    val userId = UUID.randomUUID()
    val authToken = AuthToken("test")
    val testUser = User("test","test")
    val userRoute: Route = new UserRoute(secretKey,userService,authService).route
  }

}
